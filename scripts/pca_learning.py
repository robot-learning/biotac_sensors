
import rospkg
import struct
import os
import numpy as np
from sklearn.decomposition import PCA
import pickle


# define important constants
_BT_FEATS = ['bt_E01', 'bt_E02', 'bt_E03', 'bt_E04', 'bt_E05', 'bt_E06', 'bt_E07', 'bt_E08', 
             'bt_E09', 'bt_E10','bt_E11', 'bt_E12', 'bt_E13', 'bt_E14', 'bt_E15', 'bt_E16', 
             'bt_E17', 'bt_E18', 'bt_E19',
             'bt_PAC01', 'bt_PAC02', 'bt_PAC03', 'bt_PAC04','bt_PAC05', 'bt_PAC06', 'bt_PAC07', 
             'bt_PAC08','bt_PAC09', 'bt_PAC10', 'bt_PAC11', 'bt_PAC12', 'bt_PAC13', 'bt_PAC14', 
             'bt_PAC15', 'bt_PAC16','bt_PAC17', 'bt_PAC18', 'bt_PAC19', 'bt_PAC20', 'bt_PAC21', 
             'bt_PAC22',
             'bt_PDC', 'bt_TAC', 'bt_TDC']

_BT_FEATS_SIM = ['bt_E01', 'bt_E02', 'bt_E03', 'bt_E04', 'bt_E05', 'bt_E06', 'bt_E07', 'bt_E08', 
                 'bt_E09', 'bt_E10','bt_E11', 'bt_E12', 'bt_E13', 'bt_E14', 'bt_E15', 'bt_E16', 
                 'bt_E17', 'bt_E18', 'bt_E19',
                 'bt_PAC', 'bt_PDC', 'bt_TAC', 'bt_TDC']

_BT_ELECTRODE_FEATS = _BT_FEATS[:19]
_PAC_FEATS = _BT_FEATS[19:41]


class pca_learning:
    def __init__(self):
        # get the file path to this package
        rospack = rospkg.RosPack()
        self.path = rospack.get_path('biotac_sensors')
        self.num_components = 8
        self.pca = PCA(n_components = self.num_components)

        self.raw_data = np.zeros((1,23))
        
    def read_SL_log(self,file_name):
        '''
        Read in an SL log file
        file_name - path to the SL data file to read (d0*)
        return - (data_set, var_names, var_units)
        data_set - a dictionary of the data with keys as saved variable names
        var_name - a list of variable names in order that they were saved in the log
        var_units - the units associated with the data of var_names
        '''
        try:
            data_in = file(file_name, 'rb')
        except IOError:
            return False
        # Get numerical header
        [numvars, cols, rows, freq] = [float(x) for x in data_in.readline().split()]
        numvars = int(numvars)
        cols = int(cols)
        rows = int(rows)
        # Get variable names and units
        header_line = data_in.readline().split()
        i = 0
        var_names = []
        var_units = []
        while i < len(header_line):
            var_names.append(header_line[i])
            var_units.append(header_line[i+1])
            i += 2
        data_set = {}
        for k in var_names:
            data_set[k] = []

        # Read in the rest of the binary data
        # Big endian floats
        raw_floats = struct.unpack('>'+'f'*numvars, data_in.read(4*numvars))
        data_in.close()
        # pack into the correct dictionary array
        for r in xrange(rows):
            for c in xrange(cols):
                idx = r*cols+c
                data_set[var_names[c]].append(raw_floats[idx])

        # Remove all 0 data
        try:
            N = data_set['time'].index(0,1)
            #print 'Data set has', N, 'valid data points'
            for k in data_set.keys():
                data_set[k] = data_set[k][:N]
        except ValueError:
            print 'No empty data time steps'

        for key in data_set.keys():
            data_set[key] = np.array(data_set[key])
        
        # added to only give raw biotac data
        data_set2 = {}
        for key in _BT_FEATS:
            data_set2[key] = data_set[key]
        return data_set

    def janine_to_np(self,data_set):
        ''' 
        converts dictionary type data structure to a numpy array 
        '''
        size = len(data_set['bt_E01'])
        # find mean of PAC values
        pac_mean = np.zeros(size)
        data_out = np.zeros((size,len(_BT_FEATS_SIM)))
        for key in _PAC_FEATS:
            pac_mean = pac_mean + data_set[key]
        for i in range(len(_BT_FEATS_SIM)):
            key = _BT_FEATS_SIM[i]
            if key == 'bt_PAC':
                data_out[:,i] = pac_mean
            else:
                data_out[:,i] = data_set[key]
                
        return data_out


    def get_janine_training_data(self):
        '''
        Read in the log file and extract all useful data
        '''
        log_data = open(self.path+"/data/janine_training/log_file_key.txt",'r')
        for line in log_data:
            words = line.split()
            if len(words) == 2:
                if words[0] != 'lower_until_contact' and words[0] != 'raise_hand':
                    #print words[0],words[1]
                    new_data = self.read_SL_log(self.path+"/data/janine_training/"+words[1])
                    if type(new_data) != bool:
                        new_data = self.janine_to_np(new_data)
                        if(self.raw_data.shape[0] == 1):
                            self.raw_data = new_data
                        else:
                            self.raw_data = np.append(self.raw_data,new_data,axis=0)
    def pca_analysis(self):
        self.pca.fit(self.raw_data)
        self.components = self.pca.components_


    def reduce_data(self, data):
        ''' 
        data is assumed to be a single data point ie 23 by 1 
        '''
        reduced = np.zeros(len(_BT_FEATS_SIM))
        for i in range(self.num_components):
            reduced[i] = np.dot(self.components[i],data)
        return reduced

    def load_analysis(self, name = 'current_analysis.txt'):
        '''
        loads an old analysis from the data folder
        '''
        file = open(self.path + '/data/' + name, 'rb')
        self.components = pickle.load(file)
        
    def save_analysis(self, name = 'current_analysis.txt'):
        '''
        writes to a file in the data folder an analysis
        '''
        file = open(self.path + '/data/' + name, 'wb')
        pickle.dump(self.components,file)
        
if __name__ == "__main__":
    # this is a test that loads in a bunch of data, reduces it and then saves and reloads it            
    pca = pca_learning()
    #pca.get_janine_training_data()
    #print "Uploaded janine_training data"
    #pca.pca_analysis()
    #pca.save_analysis()
    #components_calc = pca.components
    pca.load_analysis()
    print "The componet vectors"
    print pca.components
    #print pca.components == components_calc

    test_data = np.array([1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0])
    print "The reduced data"
    print pca.reduce_data(test_data)
