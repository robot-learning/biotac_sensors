#!/usr/bin/env python
# this file takes the contact dc data and outputs a probability using the sigmoid
import rospy
from std_msgs.msg import Float32MultiArray
from biotac_sensors.msg import BioTacForce
import numpy as np

class sigmoidClass:
    def __init__(self,init_node=True,loop_rate=100):
        if(init_node):
            rospy.init_node('sigmoid_node')
        rospy.Subscriber('/biotac/force_data',Float32MultiArray,self.f_cb)
        self.sg_pub=rospy.Publisher('/biotac/sigmoid',BioTacForce,queue_size=1)
        self.sg_data=BioTacForce()
        self.rate=rospy.Rate(loop_rate)
        
    def f_cb(self,msg):
        f_msg=BioTacForce()
        f_msg.force=msg
        s_msg=Float32MultiArray()
        for i in range(len(msg.data)):
            s_msg.data.append(self.sig(msg.data[i]-20.0))
        f_msg.sigmoid=s_msg
        self.sg_data=f_msg
    def sig(self,d):
        val=1.0/(1.0+np.exp(-d))
        return val


if __name__=='__main__':
    
    sg=sigmoidClass()
    while not rospy.is_shutdown():
        sg.sg_pub.publish(sg.sg_data)
        sg.rate.sleep()

