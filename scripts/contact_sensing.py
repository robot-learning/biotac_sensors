#!/usr/bin/env python
# Computes the contact positions and normals on the biotac sensors
import roslib
roslib.load_manifest('biotac_sensors')
import rospy
from biotac_sensors.msg import *
import numpy as np
import numpy.matlib
from geometry_msgs.msg import WrenchStamped, PointStamped
import tf
from std_msgs.msg import Float32MultiArray,Int16MultiArray
from biotac_sensors.srv import UpdateTare
from biotac_sensors.msg import BioTacForce

#from stl import mesh
from mpl_toolkits import mplot3d
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from scipy.linalg import norm
from rospkg import RosPack
import copy
class biotacSensing:
    def __init__(self,node_name='biotac_contact_node',loop_rate=100):
        self.got_biotac_data=False
        # initializing sensor parameters:
        self.num_electrodes=19
        self.got_initial_offsets=False
        self.elect_positions=np.zeros((19,3))
        self.update_elect_positions()
        self.pres_thresh_gain=1.005
        count=[0 for i in range(10)]
        self.contact_counters=[]
        for i in range(4):
            self.contact_counters.append(copy.deepcopy(count))
        self.contact_state_pub=rospy.Publisher('biotac/contact_state',Int16MultiArray,queue_size=1)


        self.bt_tare_data=[None for i in range(4)]
        self.IN_CONTACT=[False for i in range(4)]# ll4ma lab has 4 sensors
        self.f_data=BioTacForce()
        self.f_data.forces=[WrenchStamped() for i in range(4)]

        self.electrode_data_norm=[np.zeros(19) for i in range(4)]
        self.rad=5.5 # radius of the finger

        rospy.init_node(node_name)
        self.tare_pdc=[0.0 for i in range(4)] # tare = offset

        #self.update_tare('biotac/tare_values')
        
        self.rate=rospy.Rate(loop_rate)
        # Subscribe to biotac data:
        rospy.Subscriber('/biotac_pub',BioTacHand,self.biotac_cb)
        self.bt_pub=rospy.Publisher('/biotac_tare_pub',BioTacTareHand,queue_size=1)
        self.sensor_idx=[]
        self.tare_idx=[0,1,2,3]
        # Create publisher to publish contact position and force
        contact_pub_arr=[]
        cpt_pub_arr=[]
        force_pub_arr=[]
        fingers=['index_tip','middle_tip','ring_tip','thumb_tip']
        self.finger_frames=['index_biotac_origin','middle_biotac_origin','ring_biotac_origin','thumb_biotac_origin']
        for i in range(4):
            contact_pub_arr.append(rospy.Publisher('biotac/'+str(fingers[i])+'/force_wrench',
                                                   WrenchStamped,queue_size=1))
            cpt_pub_arr.append(rospy.Publisher('biotac/'+str(fingers[i])+'/contact_point',
                                                   PointStamped,queue_size=1))
            force_pub_arr.append(rospy.Publisher('biotac/'+str(fingers[i])+'/force',
                                                   PointStamped,queue_size=1))
        
        elect_pub_arr=[[] for i in range(4)]
        for i in range(4):
            for j in range(19):
                elect_pub_arr[i].append(rospy.Publisher('biotac/'+str(fingers[i])+'/E%02d'%j,
                                        WrenchStamped,queue_size=1))

        self.biotac_force_pub=rospy.Publisher('biotac/force_data',BioTacForce,queue_size=1)
        self.fingers=fingers
        self.contact_pub_arr=contact_pub_arr
        self.cpt_pub_arr=cpt_pub_arr
        self.force_pub_arr=force_pub_arr
        self.elect_pub_arr=elect_pub_arr
        
        
        # create a service which will update tare in rosparam:
        tare_srv = rospy.Service('biotac/update_tare', UpdateTare,self.call_tare)

        #
        
    def update_tare(self,param_string):
        if rospy.has_param(param_string):
            self.tare_pdc= rospy.get_param(param_string)
        else:
            print "Parameter missing"
            exit()

    def update_elect_positions(self):
        elect_position=np.zeros((20,4))
        elect_position[1][1] = 0.993;
        elect_position[1][2] = -4.855;
        elect_position[1][3] = -1.116;
        elect_position[2][1] = -2.700;
        elect_position[2][2] = -3.513;
        elect_position[2][3] = -3.670;
        elect_position[3][1] = -6.200;
        elect_position[3][2] = -3.513;
        elect_position[3][3] = -3.670;
        elect_position[4][1] = -8.000;
        elect_position[4][2] = -4.956;
        elect_position[4][3] = -1.116;
        elect_position[5][1] = -10.500;
        elect_position[5][2] = -3.513;
        elect_position[5][3] = -3.670;
        elect_position[6][1] = -13.400;
        elect_position[6][2] = -4.956;
        elect_position[6][3] = -1.116;
        elect_position[7][1] = 4.763;
        elect_position[7][2] = 0.000;
        elect_position[7][3] = -2.330;
        elect_position[8][1] = 3.031;
        elect_position[8][2] = -1.950;
        elect_position[8][3] = -3.330;
        elect_position[9][1] = 3.031;
        elect_position[9][2] = 1.950;
        elect_position[9][3] = -3.330;
        elect_position[10][1] = 1.299;
        elect_position[10][2] = 0.000;
        elect_position[10][3] = -4.330;
        elect_position[11][1] = 0.993;
        elect_position[11][2] = 4.855;
        elect_position[11][3] = -1.116;
        elect_position[12][1] = -2.700;
        elect_position[12][2] = 3.513;
        elect_position[12][3] = -3.670;
        elect_position[13][1] = -6.200;
        elect_position[13][2] = 3.513;
        elect_position[13][3] = -3.670;
        elect_position[14][1] = -8.000;
        elect_position[14][2] = 4.956;
        elect_position[14][3] = -1.116;
        elect_position[15][1] = -10.500;
        elect_position[15][2] = 3.513;
        elect_position[15][3] = -3.670;
        elect_position[16][1] = -13.400;
        elect_position[16][2] = 4.956;
        elect_position[16][3] = -1.116;
        elect_position[17][1] = -2.800;
        elect_position[17][2] = 0.000;
        elect_position[17][3] = -5.080;
        elect_position[18][1] = -9.800;
        elect_position[18][2] = 0.000;
        elect_position[18][3] = -5.080;
        elect_position[19][1] = -13.600;
        elect_position[19][2] = 0.000;
        elect_position[19][3] = -5.080;
        self.elect_positions=elect_position[1:20,1:4]

        # Normals:
        elect_normal=np.zeros((20,4))
        elect_normal[1][1] = 0.196;
        elect_normal[1][2] = -0.956;
        elect_normal[1][3] = -0.220;#0.220;
        elect_normal[2][1] = 0.0;
        elect_normal[2][2] = -0.692;
        elect_normal[2][3] = -0.722;
        elect_normal[3][1] = 0.0;
        elect_normal[3][2] = -0.692;
        elect_normal[3][3] = -0.722;
        elect_normal[4][1] = 0.0;
        elect_normal[4][2] = -0.976;
        elect_normal[4][3] = -0.220;
        elect_normal[5][1] = 0.0;
        elect_normal[5][2] = -0.692;#-0.976;
        elect_normal[5][3] = -0.722;#-0.220;
        elect_normal[6][1] = 0.0;#0.5;
        elect_normal[6][2] = -0.976;#0.0;
        elect_normal[6][3] = -0.220;#-0.866;
        elect_normal[7][1] = 0.5;
        elect_normal[7][2] = 0.0;
        elect_normal[7][3] = -0.866;
        elect_normal[8][1] = 0.5;
        elect_normal[8][2] = 0.0;
        elect_normal[8][3] = -0.866;
        elect_normal[9][1] = 0.5;
        elect_normal[9][2] = 0.0;
        elect_normal[9][3] = -0.866;
        elect_normal[10][1] = 0.5;
        elect_normal[10][2] = 0.0;
        elect_normal[10][3] = -0.866;
        elect_normal[11][1] = 0.196;
        elect_normal[11][2] = 0.956;
        elect_normal[11][3] = -0.220;
        elect_normal[12][1] = 0.0;
        elect_normal[12][2] = 0.692;
        elect_normal[12][3] = -0.722;
        elect_normal[13][1] = 0.0;
        elect_normal[13][2] = 0.692;
        elect_normal[13][3] = -0.722;
        elect_normal[14][1] = 0.0;
        elect_normal[14][2] = 0.976;
        elect_normal[14][3] = -0.220;
        elect_normal[15][1] = 0.0;
        elect_normal[15][2] = 0.692;
        elect_normal[15][3] = -0.722;
        elect_normal[16][1] = 0.0;
        elect_normal[16][2] = 0.976;
        elect_normal[16][3] = -0.220;
        elect_normal[17][1] = 0.0;
        elect_normal[17][2] = 0.0;
        elect_normal[17][3] = -1.000;
        elect_normal[18][1] = 0.0;
        elect_normal[18][2] = 0.0;
        elect_normal[18][3] = -1.000;
        elect_normal[19][1] = 0.0;
        elect_normal[19][2] = 0.0;
        elect_normal[19][3] = -1.000;
        #elect_normal[:,3]=elect_normal[:,3]*-1.0
        self.elect_normal=-1.0*elect_normal[1:20,1:4]
    def broadcast_bt_cpt(self,position,idx):
        br = tf.TransformBroadcaster()
        br.sendTransform((position[0],position[1],position[2]),tf.transformations.quaternion_from_euler(0,0,0),
                         rospy.Time.now(),
                         self.fingers[self.sensor_idx[idx]]+'_cpt',
                         self.finger_frames[self.sensor_idx[idx]])
    def broadcast_elect_tf(self):
        br = tf.TransformBroadcaster()
        normal_norm = np.linalg.norm(self.elect_normal,axis=1)
        self.elect_normal = self.elect_normal / np.matlib.repmat(normal_norm.reshape(19,1),1,3)
        for j in range(19):
            for i in range(4):
                br.sendTransform((self.elect_positions[j][0]/1000.0,self.elect_positions[j][1]/1000.0,self.elect_positions[j][2]/1000.0),
                                 tf.transformations.quaternion_from_euler(0, 0, 0),
                                 rospy.Time.now(),
                                 self.fingers[i] + '_E%02d'%j,
                                 self.finger_frames[i])
        
    def biotac_cb(self,msg):
        frc_data=[]
        if(not self.got_biotac_data):

            self.num_sensors=len(msg.bt_data)
            self.bio_data=[[] for i in range(self.num_sensors)]
            self.tare=[[] for i in range(self.num_sensors)]
            self.sensor_idx=[]
            #self.tare_idx=[]
            for i in range(self.num_sensors):
                self.sensor_idx.append(msg.bt_data[i].bt_position-1)
                self.tare_idx[msg.bt_data[i].bt_position-1]=i
            self.got_biotac_data=True

        tared_msg=BioTacTareHand()
        tared_msg.header.stamp=rospy.Time.now()
        tared_msg.bt_data=[BioTacTareData() for i in range(self.num_sensors)]
        tared_msg.bt_time=msg.bt_time
        c_state_msg=Int16MultiArray()
        c_state_msg.data=np.zeros(4)
        
        for i in range(self.num_sensors):
            if(self.got_initial_offsets):
                # subtract electore,pdc,pac
                tared_msg.bt_data[i].bt_serial=msg.bt_data[i].bt_serial
                tared_msg.bt_data[i].bt_position=msg.bt_data[i].bt_position
                tared_msg.bt_data[i].electrode_data = np.array(msg.bt_data[i].electrode_data)- np.array(self.bt_tare_data[i].electrode_data)
                tared_msg.bt_data[i].pac_data = np.array(msg.bt_data[i].pac_data)- np.array(self.bt_tare_data[i].pac_data)

                tared_msg.bt_data[i].pdc_data = msg.bt_data[i].pdc_data-self.bt_tare_data[i].pdc_data
                tared_msg.bt_data[i].tdc_data = msg.bt_data[i].tdc_data-self.bt_tare_data[i].tdc_data
                tared_msg.bt_data[i].tac_data = msg.bt_data[i].tac_data-self.bt_tare_data[i].tac_data

                frc_data.append(msg.bt_data[i].pdc_data-self.tare_pdc[i])
                self.contact_counters[i].pop(0)

                if( tared_msg.bt_data[i].pdc_data>=10):
                    self.contact_counters[i].append(1)
                else:
                    self.contact_counters[i].append(0)
                if(all(item==1 for item in self.contact_counters[i])):
                    self.IN_CONTACT[i]=True
                    c_state_msg.data[i]=1
                else:
                    self.IN_CONTACT[i]=False
                    c_state_msg.data[i]=0


            self.bio_data[i]=msg.bt_data[i]
        self.f_data.contact=c_state_msg
        self.contact_state_pub.publish(c_state_msg)
        if(self.got_initial_offsets):
            # publish tared bt message:
            #print tared_msg
            self.bt_pub.publish(tared_msg)
        #print msg
    def get_initial_offsets(self,idx,update_rosparam=False,param_string=''):
        self.tare[idx]=np.array(self.bio_data[idx].electrode_data)
        self.tare_pdc[idx]=copy.deepcopy(self.bio_data[idx].pdc_data)
        self.bt_tare_data[idx]=copy.deepcopy(self.bio_data[idx])
        #print 'Stored initial offsets for electrodes'
        self.got_initial_offsets=True
        if(update_rosparam):
            # update rosparam:
            rospy.set_param(param_string,self.tare_pdc)
            
    def update_tare_offsets(self,f_idx=[],update_rosparam=False,param_string=''):
        for idx in f_idx:
            self.tare[idx]=np.array(self.bio_data[idx].electrode_data)
            self.tare_pdc[idx]=copy.deepcopy(self.bio_data[idx].pdc_data)
            self.bt_tare_data[idx]=copy.deepcopy(self.bio_data[idx])
            #print 'Stored initial offsets for electrodes'
        if(update_rosparam):
            # update rosparam:
            rospy.set_param(param_string,self.tare_pdc)
        
    def compute_contact_position(self,idx,R):
        # Computes the contact position of idx sensor:
        #weights=np.zeros(self.num_electrodes)
        electrode_data=self.electrode_data_norm[idx]**2 # squared as per paper
        normalized_data=electrode_data/(np.sum(electrode_data)+1e-8)
        contact_pt=np.matrix(normalized_data)*self.elect_positions #[1x19]* [19*3]= [1x3] centroid
        contact_pt=np.ravel(contact_pt)
        v_norm=0.0
        r=R
        if(contact_pt[0]>0.0):
            for j in range(3):
                v_norm+=contact_pt[j]**2
            contact_pt=contact_pt*(r/np.sqrt(v_norm))
        else:
            j=1
            while(j<3):
                v_norm+=contact_pt[j]**2 + 1e-6 # TODO: HACK! Change this Martin!
                j+=1
            j=1
            while(j<3):
                contact_pt[j]=contact_pt[j]*(r/np.sqrt(v_norm))
                j+=1
                
        
        return np.ravel(contact_pt)#/1000.0

    def compute_contact_normal(self,idx):
        electrode_data=self.electrode_data_norm[idx]
        normal=np.matrix(electrode_data)*self.elect_normal
        normal[0]=normal[0]*0.00054
        normal[1]=normal[1]*0.00026
        normal[2]=normal[2]*0.0022

        normal_vec=1.0*np.ravel(normal)/(np.linalg.norm(normal)+1e-8)
        
    
        return np.ravel(normal_vec)#/1.0
    def compute_contact_force(self,idx):
        electrode_data=self.electrode_data_norm[idx]
        normal=np.matrix(electrode_data)*self.elect_normal
        normal=np.ravel(normal)
        normal[0]=normal[0]*0.00054
        normal[1]=normal[1]*0.00026
        normal[2]=normal[2]*0.0022
        #normal[0]=normal[0]*
        #normal_vec=1.0*np.ravel(normal)/(np.linalg.norm(normal)+1e-8)
        return np.ravel(normal)#/1.0
    
    def compute_norm_data(self,idx):
        self.electrode_data_norm[idx]=np.array(self.bio_data[idx].electrode_data-self.tare[idx],
                                                     dtype=np.float32)
        
    def pub_wrench(self,position,normal,idx):
        new_pose=WrenchStamped()
    
        new_pose.header.frame_id=self.fingers[self.sensor_idx[idx]]+'_cpt'
        new_pose.header.stamp=rospy.Time.now()
        new_pose.wrench.force.x=normal[0]
        new_pose.wrench.force.y=normal[1]
        new_pose.wrench.force.z=normal[2]
        self.contact_pub_arr[self.sensor_idx[idx]].publish(new_pose)

        # publish contact point and force vector:
        c_pt=PointStamped()
        c_pt.header.frame_id=self.finger_frames[self.sensor_idx[idx]]
        c_pt.point.x=position[0]/1000.0
        c_pt.point.y=position[1]/1000.0
        c_pt.point.z=position[2]/1000.0

        self.cpt_pub_arr[self.sensor_idx[idx]].publish(c_pt)
        self.broadcast_bt_cpt(position/1000.0,idx)
        force=PointStamped()
        force.header.frame_id=self.finger_frames[self.sensor_idx[idx]]
        force.point.x=normal[0]
        force.point.y=normal[1]
        force.point.z=normal[2]
        self.force_pub_arr[self.sensor_idx[idx]].publish(force)
        self.f_data.forces[idx]=new_pose
        

    def pub_elect_vector(self,finger_idx,elect_idx):
        new_elect_vector=WrenchStamped()
        idx=finger_idx
        new_elect_vector.header.frame_id=self.fingers[self.sensor_idx[idx]] + '_E%02d'%elect_idx
        new_elect_vector.header.stamp=rospy.Time.now()
        
        new_elect_vector.wrench.force.x=self.electrode_data_norm[self.sensor_idx[idx]][elect_idx] * self.elect_normal[elect_idx][0]
        new_elect_vector.wrench.force.y=self.electrode_data_norm[self.sensor_idx[idx]][elect_idx] * self.elect_normal[elect_idx][1]
        new_elect_vector.wrench.force.z=self.electrode_data_norm[self.sensor_idx[idx]][elect_idx] * self.elect_normal[elect_idx][2]
        #if(elect_idx==8):
        #    print self.electrode_data_norm[finger_idx][elect_idx]
        self.elect_pub_arr[self.sensor_idx[idx]][elect_idx].publish(new_elect_vector)
        
    def get_contact_data(self):
        if self.got_biotac_data:
            if not self.got_initial_offsets:
                for idx in range(self.num_sensors):
                    self.get_initial_offsets(idx)
            else:
                for idx in range(self.num_sensors):

                    self.compute_norm_data(idx)
                    if(self.IN_CONTACT[idx]):
                        position=self.compute_contact_position(idx,self.rad)
                        normal=self.compute_contact_force(idx)#+position
                    else:
                        position=np.zeros(3)
                        normal=np.zeros(3)
                    self.pub_wrench(position,normal,idx)
                    for elect_idx in range(19):
                        self.pub_elect_vector(idx,elect_idx)
                self.biotac_force_pub.publish(self.f_data)
    def get_tare_data(self):
        if self.got_biotac_data:
            if not self.got_initial_offsets:
                for idx in range(self.num_sensors):
                    self.get_initial_offsets(idx)
            else:
                for idx in range(self.num_sensors):
                    self.compute_norm_data(idx)
                    if(self.IN_CONTACT[idx]):
                        position=self.compute_contact_position(idx,self.rad)
                        normal=self.compute_contact_normal(idx)#+position
                    else:
                        position=np.zeros(3)
                        normal=np.zeros(3)
                    self.pub_wrench(position,normal,idx)

    # create service server:
    def call_tare(self,req):
        t_idx=[]
        for i in range(len(req.fingers)):
            t_idx.append(self.tare_idx[req.fingers[i]])
        self.update_tare_offsets(t_idx,True,'biotac/tare_values')
        return True

def main():
    biotac=biotacSensing()
    while not rospy.is_shutdown():
        #biotac.get_tare_data()
        if(biotac.got_initial_offsets):
            biotac.broadcast_elect_tf()
        biotac.get_contact_data()
        biotac.rate.sleep()

main()

